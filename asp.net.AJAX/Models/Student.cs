﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.net.AJAX.Models
{
    public class Student
    {
        public string Name { get; set; }
        public string Mark { get; set; }
    }
}
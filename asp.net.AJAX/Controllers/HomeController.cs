﻿using asp.net.AJAX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace asp.net.AJAX.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string Generator()
        {
            var random = new Random();
            var number = random.Next(1, 50).ToString();
            return number;
        }

        public string GetUserName(string firstname, string lastname)
        {
            return $"{firstname}&{lastname}";
        }

        [HttpPost]
        public string AddStudent(Student studentData)
        {
            var students = new List<Student>();
            students.Add(studentData);
            return "success";
        }
    }
}